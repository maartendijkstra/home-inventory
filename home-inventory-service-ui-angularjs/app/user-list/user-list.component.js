'use strict';

angular.
    module('userList').
        component('userList', {
        templateUrl: 'user-list/user-list.template.html',
        controller: function UserListController($http) {

        self = this;

        self.makeCall = function (url) {
            $http.get(url).success(function (data) {
                console.log(data)
            }).error(function (er) {
                console.log(er)
            });
        }

        self.makeCall('http://localhost:8080/users');
        }
});
