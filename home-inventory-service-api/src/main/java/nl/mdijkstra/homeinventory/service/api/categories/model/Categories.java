package nl.mdijkstra.homeinventory.service.api.categories.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.categories.subcategories.model.Subcategories;
import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.model.DetailedLocations;
import nl.mdijkstra.homeinventory.service.api.locations.model.Locations;

@Getter
@Setter
@AllArgsConstructor
public class Categories {

    private long id;
    private long name;
    private Subcategories subcategories;
    private Locations locations;
    private DetailedLocations detailedLocations;
}
