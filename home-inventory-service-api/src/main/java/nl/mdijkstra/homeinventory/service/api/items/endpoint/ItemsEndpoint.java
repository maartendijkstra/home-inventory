package nl.mdijkstra.homeinventory.service.api.items.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface ItemsEndpoint {

    @RequestMapping("/users/{id}/inventories/{id}/items")
    List<ItemsEntity> getAllItems(@PathVariable Long id);

    @RequestMapping("/users/{id}/inventories/{id}/items/{id}")
    ItemsEntity getItem(@PathVariable Long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/items")
    void addItem(@RequestBody ItemsEntity itemsEntity, @PathVariable long id, @PathVariable Long inventoryId);

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/items/{itemId}")
    void updateItem(@RequestBody ItemsEntity itemsEntity,
                           @PathVariable long id, @PathVariable long inventoryId, @PathVariable long itemId);

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/items/{itemId}")
    void deleteItem(@PathVariable long id, @PathVariable long itemId);
}

