package nl.mdijkstra.homeinventory.service.api.locations.controller;

import nl.mdijkstra.homeinventory.service.api.locations.endpoint.LocationsEndpoint;
import nl.mdijkstra.homeinventory.service.api.locations.service.LocationsService;
import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LocationsController implements LocationsEndpoint
{
    @Autowired
    LocationsService locationsService;

    @RequestMapping("/users/{id}/inventories/{id}/locations")
    public List<LocationsEntity> getAllLocations(@PathVariable long id){
        return locationsService.getAllLocations(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}")
    public LocationsEntity getLocation(@PathVariable long id) {
        return locationsService.getLocation(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/locations")
    public void addLocation(@RequestBody LocationsEntity locationsEntity, @PathVariable long id, @PathVariable long inventoryId) {
        locationsEntity.setInventory(new InventoriesEntity(inventoryId));
        locationsService.addLocation(locationsEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/locations/{locationId}")
    public void updateLocation(@RequestBody LocationsEntity locationsEntity,
                               @PathVariable long id, @PathVariable long inventoryId, @PathVariable long locationId) {
        locationsEntity.setInventory(new InventoriesEntity(inventoryId));
        locationsService.updateLocation(locationsEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/locations/{locationsId}")
    public void deleteLocation(@PathVariable long id, @PathVariable long locationsId) {
        locationsService.deleteLocation(locationsId);
    }
}

