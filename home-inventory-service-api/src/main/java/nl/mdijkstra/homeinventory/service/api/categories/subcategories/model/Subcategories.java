package nl.mdijkstra.homeinventory.service.api.categories.subcategories.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.categories.model.Categories;
import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.model.DetailedLocations;
import nl.mdijkstra.homeinventory.service.api.locations.model.Locations;

@Getter
@Setter
@AllArgsConstructor
public class Subcategories {

    private long id;
    private long name;
    private Categories categories;
    private Locations locations;
    private DetailedLocations detailedLocations;
}
