package nl.mdijkstra.homeinventory.service.api.inventories.service;

import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoriesService {

    @Autowired
    private InventoriesRepository inventoriesRepository;

    public List<InventoriesEntity> getAllInventories(long userId) {
        List<InventoriesEntity> inventoriesEntityList = new ArrayList<>();
        inventoriesRepository.findByUserId(userId).forEach(inventoriesEntityList::add);
        return inventoriesEntityList;
    }

    public InventoriesEntity getInventory(long id) {
        return inventoriesRepository.findById(id).orElse(null);
    }

    public void addInventory(InventoriesEntity inventoriesEntity) {
        inventoriesRepository.save(inventoriesEntity);
    }

    public void updateInventory(InventoriesEntity inventoriesEntity) {
    inventoriesRepository.save(inventoriesEntity);
        }

    public void deleteInventory(long id) {
    inventoriesRepository.deleteById(id);
    }
}
