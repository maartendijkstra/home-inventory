package nl.mdijkstra.homeinventory.service.api.categories.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface CategoriesEndpoint {

    @RequestMapping("/users/{id}/inventories/{id}/categories")
    List<CategoriesEntity> getAllCategories(@PathVariable long id);

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}")
    CategoriesEntity getCategory(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/categories")
    void addCategory(@RequestBody CategoriesEntity categoriesEntity,@PathVariable long id, @PathVariable long inventoryId);

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/categories/{categoryId}")
    void updateCategory(@RequestBody CategoriesEntity categoriesEntity,
                               @PathVariable long id, @PathVariable long inventoryId, @PathVariable long categoryId);

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/categories/{categoriesId}")
    void deleteCategory(@PathVariable long id, @PathVariable long categoriesId) ;

}
