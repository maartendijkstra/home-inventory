package nl.mdijkstra.homeinventory.service.api.items.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.categories.model.Categories;
import nl.mdijkstra.homeinventory.service.api.categories.subcategories.model.Subcategories;
import nl.mdijkstra.homeinventory.service.api.inventories.model.Inventories;
import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.model.DetailedLocations;
import nl.mdijkstra.homeinventory.service.api.locations.model.Locations;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class Items {

    private Long id;
    private String name;
    private Inventories inventories;
    private String photoURL;
    private String receiptURL;
    private String serialCode;
    private Date purchaseDate;
    private Date withdrawalDate;
    private int estimatedWorth;
    private int numberOfItems;
    private Categories categories;
    private Subcategories subcategories;
    private Locations locations;
    private DetailedLocations detailedLocations;
}
