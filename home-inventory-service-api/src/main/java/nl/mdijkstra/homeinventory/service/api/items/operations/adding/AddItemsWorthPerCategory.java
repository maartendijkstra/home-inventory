package nl.mdijkstra.homeinventory.service.api.items.operations.adding;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AddItemsWorthPerCategory {

    @Autowired
    private ItemsRepository itemsRepository;

    private double totalWorth;

    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}/inventories/{id}/categories/{id}/items/addedworth")
    public double addItemsEstimatedWorth(@PathVariable final Long id) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByCategoryId(id).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            totalWorth += itemsEntityList.get(i).getEstimatedWorth();
        }
        return totalWorth;
    }
}