package nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations.DetailedLocationsEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface DetailedLocationsEndpoint {

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}/detailedlocations")
    List<DetailedLocationsEntity> getAllLocations(@PathVariable long id);

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{id}")
    DetailedLocationsEntity getDetailedLocation(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{id}/locations/{locationId}/detailedlocations")
    void addDetailedLocation(@RequestBody DetailedLocationsEntity detailedLocationsEntity,
                                    @PathVariable long locationId);

    @RequestMapping(method = RequestMethod.PUT, value =
            "/users/{id}/inventories/{inventoryId}/locations/{locationId}/detailedlocations/{detailedLocationsId}")
    void updateDetailedLocation(@RequestBody DetailedLocationsEntity detailedLocationsEntity,
                                       @PathVariable long id, @PathVariable long inventoryId,
                                       @PathVariable long locationId, @PathVariable long detailedLocationsId);

    @RequestMapping(method = RequestMethod.DELETE,
            value = "/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{detailedLocationsId}")
    void deleteDetailedLocation(@PathVariable long id, @PathVariable long detailedLocationsId);

}
