package nl.mdijkstra.homeinventory.service.api.categories.service;

import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoriesService {

    @Autowired
    private CategoriesRepository categoriesRepository;

    public List<CategoriesEntity> getAllCategories(long inventoryId) {
        List<CategoriesEntity> categoriesEntityList = new ArrayList<>();
        categoriesRepository.findByInventoryId(inventoryId).forEach(categoriesEntityList::add);
        return categoriesEntityList;
    }

    public CategoriesEntity getCategory(long id) {
        return categoriesRepository.findById(id).orElse(null);
    }

    public void addCategory(CategoriesEntity categoriesEntity) {
        categoriesRepository.save(categoriesEntity);
    }

    public void updateCategory(CategoriesEntity categoriesEntity) {
        categoriesRepository.save(categoriesEntity);
    }

    public void deleteCategory(long id) {
        categoriesRepository.deleteById(id);
    }

}
