package nl.mdijkstra.homeinventory.service.api.locations.service;

import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LocationsService {

    @Autowired
    private LocationsRepository locationsRepository;

    public List<LocationsEntity> getAllLocations(long inventoryId) {
        List<LocationsEntity> locationsEntityList = new ArrayList<>();
        locationsRepository.findByInventoryId(inventoryId).forEach(locationsEntityList::add);
        return locationsEntityList;
    }

    public LocationsEntity getLocation(long id) {
        return locationsRepository.findById(id).orElse(null);
    }

    public void addLocation(LocationsEntity locationsEntity) {
        locationsRepository.save(locationsEntity);
    }

    public void updateLocation(LocationsEntity locationsEntity) {
        locationsRepository.save(locationsEntity);
    }

    public void deleteLocation(long id) {
        locationsRepository.deleteById(id);
    }
}




