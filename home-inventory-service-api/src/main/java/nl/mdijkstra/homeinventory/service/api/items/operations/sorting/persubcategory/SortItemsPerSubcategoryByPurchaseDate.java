package nl.mdijkstra.homeinventory.service.api.items.operations.sorting.persubcategory;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@SuppressWarnings("Duplicates")
@RestController
public class SortItemsPerSubcategoryByPurchaseDate {

    @Autowired
    private ItemsRepository itemsRepository;

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{id}/categories/{id}/subcategories/{id}/items/sortedbypurchasedate")
    public List sortByItemsByPurchaseDate(@PathVariable final Long id) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findBySubcategoryId(id).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = 1; j < itemsEntityList.size() - i; j++) {
                if (Comparator.nullsFirst(Date::compareTo).compare(itemsEntityList.get(j - 1).getPurchaseDate(), itemsEntityList.get(j).getPurchaseDate()) > 0) {
                    ItemsEntity temp = itemsEntityList.get(j - 1);
                    itemsEntityList.set(j - 1, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{id}/categories/{id}/subcategories/{id}/items/sortedbypurchasedatedescending")
    public List sortByItemsByPurchaseDescending(@PathVariable final Long id) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findBySubcategoryId(id).forEach(itemsEntityList::add);
        itemsEntityList = sortByItemsByPurchaseDate(id);
        for (int i = 0; i < itemsEntityList.size() / 2; i++) {
            ItemsEntity temp = itemsEntityList.get(i);
            itemsEntityList.set(i, itemsEntityList.get(itemsEntityList.size() - i - 1));
            itemsEntityList.set(itemsEntityList.size() - i - 1, temp);
        }
        return itemsEntityList;
    }
}