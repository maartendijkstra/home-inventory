package nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.controller;

import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.endpoint.DetailedLocationsEndpoint;
import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.service.DetailedLocationsService;
import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations.DetailedLocationsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DetailedLocationsController implements DetailedLocationsEndpoint {

    @Autowired
    DetailedLocationsService detailedLocationsService;

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}/detailedlocations")
    public List<DetailedLocationsEntity> getAllLocations(@PathVariable long id) {
        return detailedLocationsService.getAllDetailedLocations(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{id}")
    public DetailedLocationsEntity getDetailedLocation(@PathVariable long id) {
        return detailedLocationsService.getDetailedLocation(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{id}/locations/{locationId}/detailedlocations")
    public void addDetailedLocation(@RequestBody DetailedLocationsEntity detailedLocationsEntity,
                                    @PathVariable long locationId) {
        detailedLocationsEntity.setLocation(new LocationsEntity(locationId));
        detailedLocationsService.addDetailedLocation(detailedLocationsEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value =
            "/users/{id}/inventories/{inventoryId}/locations/{locationId}/detailedlocations/{detailedLocationsId}")
    public void updateDetailedLocation(@RequestBody DetailedLocationsEntity detailedLocationsEntity,
                               @PathVariable long id, @PathVariable long inventoryId,
                               @PathVariable long locationId, @PathVariable long detailedLocationsId) {
        detailedLocationsEntity.setLocation(new LocationsEntity(locationId));
        detailedLocationsService.updateDetailedLocation(detailedLocationsEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE,
            value = "/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{detailedLocationsId}")
    public void deleteDetailedLocation(@PathVariable long id, @PathVariable long detailedLocationsId) {
        detailedLocationsService.deleteDetailedLocation(detailedLocationsId);
    }
}
