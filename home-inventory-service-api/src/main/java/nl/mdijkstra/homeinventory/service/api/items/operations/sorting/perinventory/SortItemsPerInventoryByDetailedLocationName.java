package nl.mdijkstra.homeinventory.service.api.items.operations.sorting.perinventory;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
@RestController
public class SortItemsPerInventoryByDetailedLocationName {

    @Autowired
    private ItemsRepository itemsRepository;

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{inventoryId}/items/sortedbydetailedlocationname")
    public List sortByItemsByDetailedLocationName(@PathVariable final Long inventoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByInventoryId(inventoryId).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = 1; j < itemsEntityList.size() - i; j++) {
                if (itemsEntityList.get(j - 1).getDetailedLocation().getName().
                        compareToIgnoreCase(itemsEntityList.get(j).getDetailedLocation().getName()) > 0) {
                    ItemsEntity temp = itemsEntityList.get(j - 1);
                    itemsEntityList.set(j - 1, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{inventoryId}/items/sortedbydetailedlocationnamedescending")
    public List sortByItemsByNameDescending(@PathVariable final Long inventoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByInventoryId(inventoryId).forEach(itemsEntityList::add);
        itemsEntityList = sortByItemsByDetailedLocationName(inventoryId);
        for (int i = 0; i < itemsEntityList.size() / 2; i++) {
            ItemsEntity temp = itemsEntityList.get(i);
            itemsEntityList.set(i, itemsEntityList.get(itemsEntityList.size() - i - 1));
            itemsEntityList.set(itemsEntityList.size() - i - 1, temp);
        }
        return itemsEntityList;
    }
}