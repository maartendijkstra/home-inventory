package nl.mdijkstra.homeinventory.service.api.items.operations.sorting.perdetailedlocation;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@SuppressWarnings("Duplicates")
@RestController
public class SortItemsPerDetailedLocationByWithdrawalDate {

    @Autowired
    private ItemsRepository itemsRepository;

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{id}/items/sortedbywithdrawaldate")
    public List sortByItemsByWithdrawalDateAscending(@PathVariable final Long id) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByDetailedLocationId(id).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = 1; j < itemsEntityList.size() - i; j++) {
                if (Comparator.nullsLast(Date::compareTo).compare
                        (itemsEntityList.get(j - 1).getWithdrawalDate(), itemsEntityList.get(j).getWithdrawalDate()) > 0) {
                    ItemsEntity temp = itemsEntityList.get(j - 1);
                    itemsEntityList.set(j - 1, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/users/{id}/inventories/{id}/locations/{id}/detailedlocations/{id}/items/sortedbywithdrawaldatedescending")
    public List sortByItemsByWithdrawalDateDescending(@PathVariable final Long id) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByDetailedLocationId(id).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = 1; j < itemsEntityList.size() - i; j++) {
                if (Comparator.nullsFirst(Date::compareTo).compare
                        (itemsEntityList.get(j - 1).getWithdrawalDate(), itemsEntityList.get(j).getWithdrawalDate()) < 0) {
                    ItemsEntity temp = itemsEntityList.get(j - 1);
                    itemsEntityList.set(j - 1, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }
}