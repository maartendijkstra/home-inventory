package nl.mdijkstra.homeinventory.service.api.users.controller;

import nl.mdijkstra.homeinventory.service.api.users.endpoint.UsersEndpoint;
import nl.mdijkstra.homeinventory.service.api.users.service.UsersService;
import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController implements UsersEndpoint {

    @Autowired
    private UsersService usersService;

    @RequestMapping("/users")
      public List<UsersEntity> getAllUsers() {
        System.out.println("bla");
        return usersService.getAllUsers();
    }

    @RequestMapping("/users/{id}")
    public UsersEntity getUser(@PathVariable long id) {
        return usersService.getUser(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users")
    public void addUser(@RequestBody UsersEntity usersEntity) {
        usersService.addUser(usersEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}")
    public void updateUser(@RequestBody UsersEntity usersEntity, @PathVariable long id) {
        usersService.updateUser(id, usersEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}")
    public void deleteUser(@PathVariable long id) {
        usersService.deleteUser(id);
    }
}
