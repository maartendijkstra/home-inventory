package nl.mdijkstra.homeinventory.service.api.locations.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.categories.model.Categories;
import nl.mdijkstra.homeinventory.service.api.categories.subcategories.model.Subcategories;
import nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.model.DetailedLocations;

@Getter
@Setter
@AllArgsConstructor
public class Locations {

    private long id;
    private long name;
    private DetailedLocations detailedLocations;
    private Categories categories;
    private Subcategories subcategories;
}
