package nl.mdijkstra.homeinventory.service.api.inventories.controller;

import nl.mdijkstra.homeinventory.service.api.inventories.endpoint.InventoriesEndpoint;
import nl.mdijkstra.homeinventory.service.api.inventories.service.InventoriesService;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoriesController implements InventoriesEndpoint {

    @Autowired
    private InventoriesService inventoriesService;

    @RequestMapping("/users/{id}/inventories")
    public List<InventoriesEntity> getAllInventories(@PathVariable long id) {
        return inventoriesService.getAllInventories(id);
    }

    @RequestMapping("/users/{userId}/inventories/{id}")
    public InventoriesEntity getInventory(@PathVariable long userId, @PathVariable long id) {
        return inventoriesService.getInventory(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/inventories")
    public void addInventory(@RequestBody InventoriesEntity inventoriesEntity, @PathVariable long userId) {
        inventoriesEntity.setUser(new UsersEntity(userId));
        inventoriesService.addInventory(inventoriesEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/inventories/{id}")
    public void updateInventory(@RequestBody InventoriesEntity inventoriesEntity, @PathVariable long userId, @PathVariable long id) {
        inventoriesEntity.setUser(new UsersEntity(userId, ""));
        inventoriesService.updateInventory(inventoriesEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/inventories/{id}")
    public void deleteInventory(@PathVariable long userId, @PathVariable long id) {
        inventoriesService.deleteInventory(id);
    }

}
