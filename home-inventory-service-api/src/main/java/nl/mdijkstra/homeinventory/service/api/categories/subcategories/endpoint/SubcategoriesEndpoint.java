package nl.mdijkstra.homeinventory.service.api.categories.subcategories.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.categories.subcategories.SubcategoriesEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface SubcategoriesEndpoint {


    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subCategories")
    List<SubcategoriesEntity> getAllSubCategories(@PathVariable long id);

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subCategories/{id}")
    SubcategoriesEntity getSubCategory(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{id}/categories/{categoryId}/subCategories")
    void addSubCategory(@RequestBody SubcategoriesEntity subcategoriesEntity,
                               @PathVariable long id, @PathVariable long categoryId) ;

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{id}/categories/{inventoryId}/subCategories/{subCategoryId}")
    void updateSubCategory(@RequestBody SubcategoriesEntity subcategoriesEntity,
                                  @PathVariable long id, @PathVariable long inventoryId, @PathVariable long subCategoryId) ;

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/categories/{id}/subCategories/{id}")
    void deleteSubCategory(@PathVariable long id, @PathVariable long subcategoryid) ;

}
