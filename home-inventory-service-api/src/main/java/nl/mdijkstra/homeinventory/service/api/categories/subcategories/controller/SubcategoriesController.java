package nl.mdijkstra.homeinventory.service.api.categories.subcategories.controller;

import nl.mdijkstra.homeinventory.service.api.categories.subcategories.endpoint.SubcategoriesEndpoint;
import nl.mdijkstra.homeinventory.service.api.categories.subcategories.service.SubcategoriesService;
import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.categories.subcategories.SubcategoriesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubcategoriesController implements SubcategoriesEndpoint {


    @Autowired
    SubcategoriesService subcategoriesService;

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subcategories")
    public List<SubcategoriesEntity> getAllSubCategories(@PathVariable long id){
        return subcategoriesService.getAllSubCategories(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subcategories/{id}")
    public SubcategoriesEntity getSubCategory(@PathVariable long id) {
        return subcategoriesService.getSubCategory(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{id}/categories/{categoryId}/subcategories")
    public void addSubCategory(@RequestBody SubcategoriesEntity subcategoriesEntity,
                               @PathVariable long id, @PathVariable long categoryId) {
        subcategoriesEntity.setCategory(new CategoriesEntity(categoryId));
        subcategoriesService.addSubCategory(subcategoriesEntity);
    }

    @RequestMapping(method = RequestMethod.PUT,
            value = "/users/{id}/inventories/{id}/categories/{categoryId}/subcategories/{subCategoryId}")
    public void updateSubCategory(@RequestBody SubcategoriesEntity subcategoriesEntity,
                               @PathVariable long id, @PathVariable long categoryId, @PathVariable long subCategoryId) {
        subcategoriesEntity.setCategory(new CategoriesEntity(categoryId));
        subcategoriesService.updateSubCategory(subcategoriesEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/categories/{id}/subcategories/{subcategoriesId}")
    public void deleteSubCategory(@PathVariable long id, @PathVariable long subcategoriesId) {
        subcategoriesService.deleteSubCategory(subcategoriesId);
    }
}
