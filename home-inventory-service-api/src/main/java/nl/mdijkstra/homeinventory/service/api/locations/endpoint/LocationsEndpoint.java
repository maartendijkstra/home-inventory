package nl.mdijkstra.homeinventory.service.api.locations.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface LocationsEndpoint {

    @RequestMapping("/users/{id}/inventories/{id}/locations")
    List<LocationsEntity> getAllLocations(@PathVariable long id);

    @RequestMapping("/users/{id}/inventories/{id}/locations/{id}")
    LocationsEntity getLocation(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/locations")
    void addLocation(@RequestBody LocationsEntity locationsEntity, @PathVariable long id, @PathVariable long inventoryId);

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/locations/{locationId}")
    void updateLocation(@RequestBody LocationsEntity locationsEntity,
                               @PathVariable long id, @PathVariable long inventoryId, @PathVariable long locationId);

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/locations/{locationId}")
    public void deleteLocation(@PathVariable long id, @PathVariable long locationId);



}
