package nl.mdijkstra.homeinventory.service.api.categories.subcategories.service;

import nl.mdijkstra.homeinventory.service.persistence.categories.subcategories.SubcategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.categories.subcategories.SubcategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubcategoriesService {

    @Autowired
    private SubcategoriesRepository subcategoriesRepository;

    public List<SubcategoriesEntity> getAllSubCategories(long categoryId) {
        List<SubcategoriesEntity> subcategoriesEntityList = new ArrayList<>();
        subcategoriesRepository.findByCategoryId(categoryId).forEach(subcategoriesEntityList::add);
        return subcategoriesEntityList;
    }

    public SubcategoriesEntity getSubCategory(long id) {
        return subcategoriesRepository.findById(id).orElse(null);
    }

    public void addSubCategory(SubcategoriesEntity subcategoriesEntity) {
        subcategoriesRepository.save(subcategoriesEntity);
    }

    public void updateSubCategory(SubcategoriesEntity subcategoriesEntity) {
        subcategoriesRepository.save(subcategoriesEntity);
    }

    public void deleteSubCategory(long id) {
        subcategoriesRepository.deleteById(id);
    }
}
