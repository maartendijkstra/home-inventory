package nl.mdijkstra.homeinventory.service.api.users.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Users {

private Long id;
private String name;
}
