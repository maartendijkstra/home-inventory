package nl.mdijkstra.homeinventory.service.api.inventories.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface InventoriesEndpoint {

    @RequestMapping("/users/{id}/inventories")
    List<InventoriesEntity> getAllInventories(@PathVariable long id);

    @RequestMapping("/users/{userId}/inventories/{id}")
    public InventoriesEntity getInventory(@PathVariable long userId, @PathVariable long id);

    @RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/inventories")
    void addInventory(@RequestBody InventoriesEntity inventoriesEntity, @PathVariable long userId);

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/inventories/{id}")
    void updateInventory(@RequestBody InventoriesEntity inventoriesEntity, @PathVariable long userId, @PathVariable long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/inventories/{id}")
    public void deleteInventory(@PathVariable long userId, @PathVariable long id);
}
