package nl.mdijkstra.homeinventory.service.api.users.service;

import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;
import nl.mdijkstra.homeinventory.service.persistence.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    private UsersEntity usersEntity;

    public List<UsersEntity> getAllUsers() {
        List<UsersEntity> usersEntityList = new ArrayList<>();
        usersRepository.findAll().forEach(usersEntityList::add);
        return usersEntityList;
    }

    public UsersEntity getUser(long id) {
        return usersRepository.findById(id).orElse(null);
    }

    public void addUser(UsersEntity usersEntity) {
        usersRepository.save(usersEntity);
    }

    public void updateUser(long id, UsersEntity usersEntity) {
    usersRepository.save(usersEntity);
        }

    public void deleteUser(long id) {
    usersRepository.deleteById(id);
    }
}
