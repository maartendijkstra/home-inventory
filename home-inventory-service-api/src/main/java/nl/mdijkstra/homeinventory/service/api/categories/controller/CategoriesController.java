package nl.mdijkstra.homeinventory.service.api.categories.controller;

import nl.mdijkstra.homeinventory.service.api.categories.endpoint.CategoriesEndpoint;
import nl.mdijkstra.homeinventory.service.api.categories.service.CategoriesService;
import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoriesController implements CategoriesEndpoint {

    @Autowired
    CategoriesService categoriesService;

    @RequestMapping("/users/{id}/inventories/{id}/categories")
    public List<CategoriesEntity> getAllCategories(@PathVariable long id){
        return categoriesService.getAllCategories(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}")
    public CategoriesEntity getCategory(@PathVariable long id) {
        return categoriesService.getCategory(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/categories")
    public void addCategory(@RequestBody CategoriesEntity categoriesEntity,@PathVariable long id, @PathVariable long inventoryId) {
        categoriesEntity.setInventory(new InventoriesEntity(inventoryId));
        categoriesService.addCategory(categoriesEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/categories/{categoryId}")
    public void updateCategory(@RequestBody CategoriesEntity categoriesEntity,
                               @PathVariable long id, @PathVariable long inventoryId, @PathVariable long categoryId) {
        categoriesEntity.setInventory(new InventoriesEntity(inventoryId));
        categoriesService.updateCategory(categoriesEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/categories/{categoriesId}")
    public void deleteCategory(@PathVariable long id, @PathVariable long categoriesId) {
        categoriesService.deleteCategory(categoriesId);
    }


}
