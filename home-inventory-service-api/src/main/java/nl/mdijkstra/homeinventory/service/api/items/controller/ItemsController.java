package nl.mdijkstra.homeinventory.service.api.items.controller;

import nl.mdijkstra.homeinventory.service.api.items.endpoint.ItemsEndpoint;
import nl.mdijkstra.homeinventory.service.api.items.service.ItemsService;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ItemsController implements ItemsEndpoint {

    @Autowired
    private ItemsService itemsService;

    @RequestMapping("/users/{id}/inventories/{id}/items")
    public List<ItemsEntity> getAllItems(@PathVariable Long id) {
        return itemsService.getAllItems(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/items")
    public List<ItemsEntity> getAllItemsPerCategory(@PathVariable Long id) {
        return itemsService.getAllItemsPerCategory(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subcategories/{id}/items")
    public List<ItemsEntity> getAllItemsPerSubcategory(@PathVariable Long id) {
        return itemsService.getAllItemsPerSubcategory(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/location/{id}/items")
    public List<ItemsEntity> getAllItemsPerLocation(@PathVariable Long id) {
        return itemsService.getAllItemsPerLocation(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/location/{id}/detailedlocation/{id}/items")
    public List<ItemsEntity> getAllItemsPerDetailedLocation(@PathVariable Long id) {
        return itemsService.getAllItemsPerDetailedLocation(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/items/{id}")
    public ItemsEntity getItem(@PathVariable Long id) {
        return itemsService.getItem(id);
    }


    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/items/{id}")
    public ItemsEntity getItemPerCategory(@PathVariable Long id) {
        return itemsService.getItemPerCategory(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/categories/{id}/subcategory/{id}/items/{id}")
    public ItemsEntity getItemPerSubcategory(@PathVariable Long id) {
        return itemsService.getItemPerSubcategory(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/location/{id}/items/{id}")
    public ItemsEntity getItemPerLocation(@PathVariable Long id) {
        return itemsService.getItemPerLocation(id);
    }

    @RequestMapping("/users/{id}/inventories/{id}/location/{id}/detailedlocation/{id{}/items/{id}")
    public ItemsEntity getItemPerDetailedLocation(@PathVariable Long id) {
        return itemsService.getItemPerDetailedLocation(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{id}/inventories/{inventoryId}/items")
    public void addItem(@RequestBody ItemsEntity itemsEntity, @PathVariable long id, @PathVariable Long inventoryId) {
        itemsEntity.setInventory(new InventoriesEntity(inventoryId));
        itemsService.addItem(itemsEntity);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}/inventories/{inventoryId}/items/{itemId}")
    public void updateItem(@RequestBody ItemsEntity itemsEntity,
                           @PathVariable long id, @PathVariable long inventoryId, @PathVariable long itemId) {
        itemsEntity.setInventory(new InventoriesEntity(inventoryId));
        itemsService.updateItem(itemsEntity);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}/inventories/{id}/items/{itemId}")
    public void deleteItem(@PathVariable long id, @PathVariable long itemId) {
        itemsService.deleteItem(itemId);
    }

}
