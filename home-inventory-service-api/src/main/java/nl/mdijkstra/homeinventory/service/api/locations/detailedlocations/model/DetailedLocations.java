package nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.categories.model.Categories;
import nl.mdijkstra.homeinventory.service.api.categories.subcategories.model.Subcategories;
import nl.mdijkstra.homeinventory.service.api.locations.model.Locations;

@Getter
@Setter
@AllArgsConstructor
public class DetailedLocations {

    private long id;
    private long name;
    private Locations locations;
    private Categories categories;
    private Subcategories subcategories;


}
