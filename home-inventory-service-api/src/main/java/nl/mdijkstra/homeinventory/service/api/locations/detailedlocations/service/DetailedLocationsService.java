package nl.mdijkstra.homeinventory.service.api.locations.detailedlocations.service;

import nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations.DetailedLocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations.DetailedLocationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DetailedLocationsService {

    @Autowired
    private DetailedLocationsRepository detailedLocationsRepository;

    public List<DetailedLocationsEntity> getAllDetailedLocations(long locationId) {
        List<DetailedLocationsEntity> detailedLocationsEntityList = new ArrayList<>();
        detailedLocationsRepository.findByLocationId(locationId).forEach(detailedLocationsEntityList::add);
        return detailedLocationsEntityList;
    }

    public DetailedLocationsEntity getDetailedLocation(long id) {
        return detailedLocationsRepository.findById(id).orElse(null);
    }

    public void addDetailedLocation(DetailedLocationsEntity detailedLocationsEntity) {
        detailedLocationsRepository.save(detailedLocationsEntity);
    }

    public void updateDetailedLocation(DetailedLocationsEntity detailedLocationsEntity) {
        detailedLocationsRepository.save(detailedLocationsEntity);
    }

    public void deleteDetailedLocation(long id) {
        detailedLocationsRepository.deleteById(id);
    }

}
