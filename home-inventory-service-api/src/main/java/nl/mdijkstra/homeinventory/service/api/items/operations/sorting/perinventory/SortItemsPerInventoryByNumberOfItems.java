package nl.mdijkstra.homeinventory.service.api.items.operations.sorting.perinventory;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
@RestController
public class SortItemsPerInventoryByNumberOfItems {

    @Autowired
    private ItemsRepository itemsRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}/inventories/{inventoryId}/items/sortedbynumberofitems")
    public List sortByItemsByNumberOfItemsAscending(@PathVariable final Long inventoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByInventoryId(inventoryId).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = itemsEntityList.size() - 1; j > i; j--) {
                if (itemsEntityList.get(i).getNumberOfItems() > itemsEntityList.get(j).getNumberOfItems()) {
                    ItemsEntity temp = itemsEntityList.get(i);
                    itemsEntityList.set(i, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}/inventories/{inventoryId}/items/sortedbynumberofitemsdescending")
    public List sortByItemsByNumberOfItemsDescending(@PathVariable final Long inventoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByInventoryId(inventoryId).forEach(itemsEntityList::add);
        for (int i = 0; i < itemsEntityList.size(); i++) {
            for (int j = itemsEntityList.size() - 1; j > i; j--) {
                if (itemsEntityList.get(i).getNumberOfItems() < itemsEntityList.get(j).getNumberOfItems()) {
                    ItemsEntity temp = itemsEntityList.get(i);
                    itemsEntityList.set(i, itemsEntityList.get(j));
                    itemsEntityList.set(j, temp);
                }
            }
        }
        return itemsEntityList;
    }
}
