package nl.mdijkstra.homeinventory.service.api.items.service;

import nl.mdijkstra.homeinventory.service.persistence.items.ItemsEntity;
import nl.mdijkstra.homeinventory.service.persistence.items.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemsService {

    @Autowired
    private ItemsRepository itemsRepository;

    public List<ItemsEntity> getAllItems(Long inventoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByInventoryId(inventoryId).forEach(itemsEntityList::add);
        return itemsEntityList;
    }

    public List<ItemsEntity> getAllItemsPerCategory(Long categoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByCategoryId(categoryId).forEach(itemsEntityList::add);
        return itemsEntityList;
    }

        public List<ItemsEntity> getAllItemsPerSubcategory(Long subcategoryId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findBySubcategoryId(subcategoryId).forEach(itemsEntityList::add);
        return itemsEntityList;
    }

    public List<ItemsEntity> getAllItemsPerLocation(Long locationId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByCategoryId(locationId).forEach(itemsEntityList::add);
        return itemsEntityList;
    }

    public List<ItemsEntity> getAllItemsPerDetailedLocation(Long detailedLocationId) {
        List<ItemsEntity> itemsEntityList = new ArrayList<>();
        itemsRepository.findByCategoryId(detailedLocationId).forEach(itemsEntityList::add);
        return itemsEntityList;
    }

    public ItemsEntity getItem(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }

    public ItemsEntity getItemPerCategory(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }

    public ItemsEntity getItemPerSubcategory(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }

    public ItemsEntity getItemPerLocation(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }

    public ItemsEntity getItemPerDetailedLocation(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }

    public void addItem(ItemsEntity itemsEntity) {
        itemsRepository.save(itemsEntity);
    }

    public void updateItem(ItemsEntity itemsEntity) {
    itemsRepository.save(itemsEntity);
        }

    public void deleteItem(Long id) {
    itemsRepository.deleteById(id);
    }
}
