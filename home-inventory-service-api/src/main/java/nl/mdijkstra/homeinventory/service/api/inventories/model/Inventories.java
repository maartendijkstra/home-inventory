package nl.mdijkstra.homeinventory.service.api.inventories.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.api.users.model.Users;

@Getter
@Setter
@AllArgsConstructor
public class Inventories {

private Long id;
private String name;
private Users users;
}
