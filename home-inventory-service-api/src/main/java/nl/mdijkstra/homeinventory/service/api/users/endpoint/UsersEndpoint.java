package nl.mdijkstra.homeinventory.service.api.users.endpoint;

import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface UsersEndpoint {

    @RequestMapping("/users")
    List<UsersEntity> getAllUsers();

    @RequestMapping("/users/{id}")
    UsersEntity getUser(@PathVariable long id) ;

    @RequestMapping(method = RequestMethod.POST, value = "/users")
    void addUser(@RequestBody UsersEntity usersEntity);

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{id}")
    void updateUser(@RequestBody UsersEntity usersEntity, @PathVariable long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{id}")
    void deleteUser(@PathVariable long id);
}
