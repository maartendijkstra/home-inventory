package nl.mdijkstra.homeinventory.service.persistence.items;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.categories.subcategories.SubcategoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;
import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations.DetailedLocationsEntity;
import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Item")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemsEntity {

    @GeneratedValue
    @Id
    @Column
    private Long id;
    private String name;
    private String photoURL;
    private String receiptURL;
    private String serialCode;
    private Date purchaseDate;
    private Date withdrawalDate;
    private double estimatedWorth;
    private int numberOfItems;
    @ManyToOne
    private UsersEntity user;
    @ManyToOne
    private InventoriesEntity inventory;
    @ManyToOne
    private CategoriesEntity category;
    @ManyToOne
    SubcategoriesEntity subcategory;
    @ManyToOne
    private LocationsEntity location;
    @ManyToOne
    private DetailedLocationsEntity detailedLocation;


    public ItemsEntity(Long id){
        this.id = id;
    }
}
