package nl.mdijkstra.homeinventory.service.persistence.inventories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.users.UsersEntity;

import javax.persistence.*;

@Entity
@Table(name = "Inventory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InventoriesEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    @ManyToOne
    private UsersEntity user;

    public InventoriesEntity(Long id) {
        this.id = id;
    }
}
