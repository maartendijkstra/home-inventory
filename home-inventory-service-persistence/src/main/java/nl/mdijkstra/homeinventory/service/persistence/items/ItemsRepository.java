package nl.mdijkstra.homeinventory.service.persistence.items;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsRepository extends CrudRepository<ItemsEntity, Long> {

    List<ItemsEntity> findByInventoryId(Long inventoryId);

    List<ItemsEntity> findByCategoryId(Long categoryId);

    List<ItemsEntity> findBySubcategoryId(Long subcategoryId);

    List<ItemsEntity> findByLocationId(Long locationId);

    List<ItemsEntity> findByDetailedLocationId(Long detailedLocationId);
}
