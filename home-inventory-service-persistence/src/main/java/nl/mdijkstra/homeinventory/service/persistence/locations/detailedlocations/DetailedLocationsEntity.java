package nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.locations.LocationsEntity;

import javax.persistence.*;

@Entity
@Table(name = "DetailedLocation")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DetailedLocationsEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    @ManyToOne
    private LocationsEntity location;

    public DetailedLocationsEntity(Long id){
        this.id = id;
    }
}
