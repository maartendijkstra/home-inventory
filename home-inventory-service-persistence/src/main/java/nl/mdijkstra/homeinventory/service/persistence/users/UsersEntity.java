package nl.mdijkstra.homeinventory.service.persistence.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "User")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsersEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;

    public UsersEntity(long id){
        this.id=id;
    }

}
