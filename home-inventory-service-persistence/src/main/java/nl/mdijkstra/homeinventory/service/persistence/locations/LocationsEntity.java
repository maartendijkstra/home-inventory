package nl.mdijkstra.homeinventory.service.persistence.locations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;

import javax.persistence.*;

@Entity
@Table(name = "Location")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocationsEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    @ManyToOne
    private InventoriesEntity inventory;

    public LocationsEntity(long id){
        this.id = id;
    }


}
