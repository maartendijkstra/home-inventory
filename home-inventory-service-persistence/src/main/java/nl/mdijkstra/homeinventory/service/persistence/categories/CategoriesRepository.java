package nl.mdijkstra.homeinventory.service.persistence.categories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends CrudRepository <CategoriesEntity, Long> {

    List<CategoriesEntity> findByInventoryId(Long inventoryId);
}
