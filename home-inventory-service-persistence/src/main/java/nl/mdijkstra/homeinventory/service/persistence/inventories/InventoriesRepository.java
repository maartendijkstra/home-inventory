package nl.mdijkstra.homeinventory.service.persistence.inventories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoriesRepository extends CrudRepository<InventoriesEntity, Long> {

    List<InventoriesEntity> findByUserId(Long userId);
}
