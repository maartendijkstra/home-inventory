package nl.mdijkstra.homeinventory.service.persistence.locations;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationsRepository extends CrudRepository<LocationsEntity, Long> {

    List<LocationsEntity> findByInventoryId(Long inventoryId);

}
