package nl.mdijkstra.homeinventory.service.persistence.categories.subcategories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubcategoriesRepository extends CrudRepository<SubcategoriesEntity, Long> {

    List<SubcategoriesEntity> findByCategoryId(Long categoryId);
}
