package nl.mdijkstra.homeinventory.service.persistence.categories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.inventories.InventoriesEntity;

import javax.persistence.*;

@Entity
@Table(name = "Category")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoriesEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    @ManyToOne
    private InventoriesEntity inventory;

    public CategoriesEntity(Long id){
        this.id = id;
    }
}
