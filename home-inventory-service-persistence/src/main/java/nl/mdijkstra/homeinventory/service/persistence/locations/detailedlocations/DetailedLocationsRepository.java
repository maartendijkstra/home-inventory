package nl.mdijkstra.homeinventory.service.persistence.locations.detailedlocations;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailedLocationsRepository extends CrudRepository<DetailedLocationsEntity, Long> {

    List<DetailedLocationsEntity> findByLocationId(Long locationId);
}
