package nl.mdijkstra.homeinventory.service.persistence.categories.subcategories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.mdijkstra.homeinventory.service.persistence.categories.CategoriesEntity;

import javax.persistence.*;

@Entity
@Table(name = "Subcategory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubcategoriesEntity {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    @ManyToOne
    private CategoriesEntity category;

    public SubcategoriesEntity(Long id){
        this.id = id;
    }



}
